import java.io.*;

//C:/Users/Anton/Desktop/umbers.txt
/*
Программа выводит на консоль числа из файла, отсортированные по возрастанию и убыванию.
а так же выводит факториал 20
 */
public class Task {
    public static void main(String[] args) throws IOException {

        try {
            workFileNumbers();
        }catch(FileNotFoundException e) {
            System.out.println("Файл с указанным именем не найден");
        }
        catch (NullPointerException e){
            System.out.println("Фаил пустой");
        }
        catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Файл содержит более 20 чисел");
        }
        catch (NumberFormatException e){
            System.out.println("Файл содержит недопустимые для выполенния символы");
        }



        factorial();
    }

    private static void workFileNumbers() throws  IOException, NumberFormatException{



        System.out.println("Введите имя файла:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        InputStream in = new FileInputStream(reader.readLine());

        int c;
        String word = "";
        while ((c = in.read()) != -1){
            word += (char)c;
        }
        in.close();
        reader.close();

        String process = "";
        int arr[] = new int[20];
        int j = 0;

        for(int i = 0; i < word.length(); i++){
            if(word.charAt(i) == ',' || word.charAt(i) == ','){
                arr[j] = Integer.parseInt(process);
                j++;
                process = "";
            }
            else if(word.charAt(i) == '0' || word.charAt(i) == '1' || word.charAt(i) == '2' ||word.charAt(i) == '3'
                    ||word.charAt(i) == '4' ||word.charAt(i) == '5' ||word.charAt(i) == '6' ||word.charAt(i) == '7'
                    ||word.charAt(i) == '8' ||word.charAt(i) == '9' ){
                process += word.charAt(i);
            }
            else {
                throw new NumberFormatException();
            }
        }


        sortArray(arr);
        printArray(arr);

    }

    private static void sortArray(int[] arr){
        for(int z = arr.length - 1; z > 0; z--) {
            for (int j = 0; j < z; j++) {
                if (arr[j] > arr[j + 1]) {
                    arr[j + 1] += arr[j];
                    arr[j] = arr[j + 1] - arr[j];
                    arr[j + 1] -= arr[j];
                }

            }
        }
    }

    private static void printArray(int[] arr){
        System.out.println("числа по возрастанию");
        for(int j = 0; j < arr.length; j++){
            System.out.println(arr[j]);
        }

        System.out.println("числа по убыванию");
        for(int j = arr.length - 1; j >= 0; j--){
            System.out.println(arr[j]);
        }
    }

    private static void factorial(){
        long x = 1;

        for (int i = 1; i <= 20; i++) {
            x = x * i;
        }
        System.out.println("Факториал 20 = " + x);
    }

}
